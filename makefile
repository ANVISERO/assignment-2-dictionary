ASM=nasm
ASMFLAGS=-f elf64
LD=ld
	
lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
	
main: main.o dict.o lib.o
	$(LD) -o $@ $^

.PHONY:
	clean
clean:
	rm *.o
	rm main