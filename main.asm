%include "lib.inc"
%include "dist.inc"
section .data
%include "words.inc"
buffer: times 255 db 0
too_long: db "string is too long", 0
not_found: db "string is not found", 0

section .text
global _start
_start:
    mov  rdi,  buffer
    mov  rsi,  255
    push rdi
    call read_word   ; считываем строку
    pop  rdi
    cmp  rax,  0
    je   .too_long      ; если строка очень длинная 
    mov  rsi,  MARK
    call find_word      ; ищем строку в словаре
    cmp  rax,  0
    je   .not_found     ; если не нашли строку
    add  rax,  8
    push rax
    mov  rdi,  rax      
    call string_length  ; считаем длину ключа
    pop  rcx
    lea  rdi,  [rcx + rax + 1]      ; получем значение объекта
    call print_string       ; выводим значение
    call exit
    .not_found:
        mov  rdi,  not_found
        call print_error
        call exit
    .too_long:
        mov  rdi,  too_long
        call print_error
        call exit