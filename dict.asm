section .text
global find_word
extern string_equals

;принимает rdi - указатель на нуль-терминированную строку
;принимает rsi - указатель на начало словаря
find_word:
    mov  rcx,  rsi      ;rcx - указатель на текущий объект
    .loop:
        lea  rsi,  [rcx + 8]        ;[rcx + 8] - ключ
        push rcx
        push rdi
        call string_equals
        pop  rdi
        pop  rcx
        cmp  rax,  1
        je   .quit
        cmp  qword[rcx],  0
        je   .error
        mov  rcx,  [rcx]
        jmp  .loop
    .quit:
        mov  rax,  rcx
        ret
    .error:
        mov  rax,  0
        ret
